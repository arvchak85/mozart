Mozart is a public library to use against http get requests. it uses volley as request layer. 
The idea behind this library is to combine the advantage of other  useful libraries together 
to make a full fledged framework. It has a caching capability. 

Currently caches the responses with no intention , but in future this will extended 
to only if the http response headers has if_modified_then parameter.