package com.framework.mozart;

import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.framework.mozart.error.MozartError;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

/**
 * Image Data Loader helps to cache the images to the LRU cache and loads it
 * from cache when it is needed.
 * 
 * @author arvind
 *
 */
public class ImageDataLoader extends DataLoader<Bitmap> {

	private String url;
	private int height;
	private int width;
	private Context mContext;
	private IResponse<Bitmap> responseListener;
	private ImageRequest request;

	public ImageDataLoader(Context context, String url, boolean needsCache,
			int width, int height) {
		super(context, url, needsCache);
		this.url = url;
		this.width = width;
		this.height = height;
		this.mContext = context;

	}

	/**
	 * This method must be called from the client to start the request.
	 */
	public void build() {

		load();
	}

	/**
	 * Cancel request will cancel the currently running and any future requests
	 * and change the status of the loader's isCancelled as true this method
	 * will call the onCancelled() method where developers can choose to do
	 * something when the request is cancelled.
	 */
	@Override
	protected void cancelRequest() {

		super.cancelRequest();
		if (request != null) {
			request.cancel();
		}
	}

	/**
	 * This method is called from the load method, This method will be called
	 * only if the data is not in cache. publishResults will be called
	 * otherwise.
	 */
	@Override
	public void sendRequest() {

		request = new ImageRequest(this.url, jsonResponseListener, this.width,
				this.height, Config.ARGB_8888, errorListener);
		RequestQueue requestQueue = Volley.newRequestQueue(mContext);
		requestQueue.add(request);

	}

	/**
	 * Success Listeners for volley request.
	 */
	private Listener<Bitmap> jsonResponseListener = new Listener<Bitmap>() {

		@Override
		public void onResponse(Bitmap object) {

			onPostExecute(object);

		}

	};
	/**
	 * Error Listeners for volley request.
	 */
	private ErrorListener errorListener = new ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError error) {
			onFailed(error);
		}
	};

	/**
	 * This method is called whenever the data is ready to be server to the UI.
	 */
	@Override
	public void publishResults(Bitmap response) {

		if (responseListener != null) {
			responseListener.OnSuccess(response);
		}

	}

	@Override
	public void onFailed(VolleyError error) {

		MozartError customError = new MozartError();
		if (error != null) {
			if (error.getMessage() != null) {
				customError.message = error.getMessage();
				if (error.networkResponse != null) {
					customError.statusCode = error.networkResponse.statusCode;
				}
			}
		}

		if (responseListener != null) {
			responseListener.OnError(customError);
		}

	}

	@Override
	public void onCancelled() {

	}

	public IResponse<Bitmap> getResponseListener() {
		return responseListener;
	}

	public void setResponseListener(IResponse<Bitmap> responseListener) {
		this.responseListener = responseListener;
	}

}
