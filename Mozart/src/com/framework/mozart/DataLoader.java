package com.framework.mozart;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.framework.mozart.error.MozartError;

/**
 * Data Loader is an abstract class that helps the use of loading any type od
 * data on the UI
 * 
 * @author arvind
 * 
 */
public abstract class DataLoader<T> {

	private boolean mCancelled;
	private Context mContext;
	private RequestQueue mQueue;
	private String url;
	private LruCache<String, T> cache;
	private boolean needCache;

	public abstract void onCancelled();

	public abstract void sendRequest();

	public abstract void onFailed(VolleyError error);

	public abstract void publishResults(T response);

	/**
	 * 
	 * @param context
	 *            : context of the activity
	 * @param url
	 *            : Web url to make a request.
	 * @param needsCache
	 *            : developer can choose to use cache or not, in future this can
	 *            be clubbed with the headers from response to check if the data
	 *            has changed in the server
	 */
	public DataLoader(Context context, String url, boolean needsCache) {
		this.mContext = context;
		this.url = url;
		int cacheSize = 4 * 1024 * 1024; // 4MB
		if (needsCache) {
			this.cache = new LruCache<String, T>(cacheSize);
			this.needCache = needsCache;
		}

	}

	/**
	 * The load method will automate the process of either starting a fresh
	 * requests or publishing the response directly if the data is available in
	 * cache.
	 */
	protected void load() {
		mCancelled = false;
		if (needCache) {
			if (cache.get(this.url) == null) {
				sendRequest();
			} else {
				T data = cache.get(this.url);
				publishResults(data);
			}
		} else {
			sendRequest();
		}

	}

	/**
	 * This method must be called from the child class to deliver the response
	 * to the UI
	 * 
	 * @param response
	 */
	protected void onPostExecute(T response) {
		if (needCache) {
			if (cache.get(this.url) == null) {
				if (response != null) {
					cache.put(this.url, response);
					publishResults(response);
				}
			} else {
				T data = cache.get(this.url);
				publishResults(data);
			}
		} else {

			if (response != null) {
				publishResults(response);
			}
		}
	}

	/**
	 * Does not cancel the request , child class must decide based on the
	 * cancelled status.
	 */
	protected void cancelRequest() {
		mCancelled = true;
		onCancelled();
	}

	protected Context getContext() {
		return this.mContext;
	}

	protected String getUrl() {
		return this.url;
	}
}
