package com.framework.mozart;

import com.framework.mozart.error.MozartError;

public interface IResponse<T> {

	public void OnError(MozartError error);

	public void OnSuccess(T response);

}
