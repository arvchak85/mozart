package com.framework.mozart;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.framework.mozart.error.MozartError;

/**
 * Json Data Loader helps to cache the json to the LRU cache and loads it from
 * cache when it is needed.
 **/
public class JsonDataLoader extends DataLoader<JSONObject> {

	private IResponse<JSONObject> responseListener;
	private JsonObjectRequest jsonRequest;

	public JsonDataLoader(Context context, String url, boolean needsCache) {

		super(context, url, needsCache);

	}

	@Override
	public void onCancelled() {

	}

	/**
	 * This method is called from the load method, This method will be called
	 * only if the data is not in cache. publishResults will be called
	 * otherwise.
	 */
	@Override
	public void sendRequest() {

		jsonRequest = new JsonObjectRequest(Request.Method.GET, getUrl(), null,
				jsonResponseListener, errorListener);
		RequestQueue requestQueue = Volley.newRequestQueue(getContext());
		requestQueue.add(jsonRequest);

	}

	private Listener<JSONObject> jsonResponseListener = new Listener<JSONObject>() {

		@Override
		public void onResponse(JSONObject object) {

			onPostExecute(object);

		}

	};

	/**
	 * This method must be called from the client to start the request.
	 */
	public void build() {
		load();
	}

	/**
	 * Cancel request will cancel the currently running and any future requests
	 * and change the status of the loader's isCancelled as true this method
	 * will call the onCancelled() method where developers can choose to do
	 * something when the request is cancelled.
	 */
	@Override
	protected void cancelRequest() {
		super.cancelRequest();
		if (jsonRequest != null) {
			jsonRequest.cancel();
		}

	}
	/**
	 * Error Listeners for volley request.
	 */
	private ErrorListener errorListener = new ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError arg0) {

		}
	};

	@Override
	public void onFailed(VolleyError error) {
		MozartError customError = new MozartError();
		if (error != null) {
			if (error.getMessage() != null) {
				customError.message = error.getMessage();
				if (error.networkResponse != null) {
					customError.statusCode = error.networkResponse.statusCode;
				}
			}
		}
		if (responseListener != null) {
			responseListener.OnError(customError);
		}

	}

	/**
	 * This method is called whenever the data is ready to be server to the UI.
	 */
	@Override
	public void publishResults(JSONObject response) {

		if (responseListener != null) {
			responseListener.OnSuccess(response);
		}

	}

	public IResponse<JSONObject> getResponseListener() {
		return responseListener;
	}

	public void setResponseListener(IResponse<JSONObject> responseListener) {
		this.responseListener = responseListener;
	}

}
