package com.sample.pinterest;

import java.util.ArrayList;

import org.json.JSONObject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.framework.mozart.IResponse;
import com.framework.mozart.JsonDataLoader;
import com.framework.mozart.error.MozartError;
import com.sample.pinterest.adapter.ProductAdapter;
import com.sample.pinterest.data.DataHelper;
import com.sample.pinterest.data.ProductData;

public class MainActivity extends AppCompatActivity implements
		IResponse<JSONObject> {

	private StaggeredGridView gridView;
	private ArrayList<ProductData> productList;
	private ProductAdapter adapter;
	private ProgressBar pbLoading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gridView = (StaggeredGridView) findViewById(R.id.grid_view);
		pbLoading = (ProgressBar) findViewById(R.id.load_more_progressBar);
		productList = new ArrayList<ProductData>();
		adapter = new ProductAdapter(this);
		gridView.setAdapter(adapter);
		String url = "https://www.zalora.com.my/mobile-api/women/";

		JsonDataLoader dataLoader = new JsonDataLoader(this, url, true);
		pbLoading.setVisibility(View.VISIBLE);
		dataLoader.setResponseListener(this);
		dataLoader.build();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void OnError(MozartError error) {
		pbLoading.setVisibility(View.GONE);
		if (error != null) {
			if (error.message != null)
				Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void OnSuccess(JSONObject response) {
		pbLoading.setVisibility(View.GONE);

		DataHelper helper = new DataHelper();
		productList = helper.buildData(response);
		for (ProductData data : productList) {
			Log.d("data", data.description);
		}
		if (adapter != null) {
			adapter.setItems(productList);
		}

	}
}
