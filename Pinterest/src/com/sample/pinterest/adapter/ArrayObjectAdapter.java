package com.sample.pinterest.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * 
 * @author arvind.rajan
 * 
 * @param <T>
 */
public abstract class ArrayObjectAdapter<T> extends BaseAdapter {
	/**
	 * Contains the list of objects that represent the data of this
	 * ArrayAdapter. The content of this list is referred to as "the array" in
	 * the documentation.
	 */
	volatile protected ArrayList<T> mObjects;

	/**
	 * The resource indicating what views to inflate to display the content of
	 * this array adapter.
	 */
	private int mResource;

	/**
	 * If the inflated resource is not a TextView, {@link #mFieldId} is used to
	 * find a TextView inside the inflated views hierarchy. This field must
	 * contain the identifier that matches the one defined in the resource file.
	 */

	/**
	 * Indicates whether or not {@link #notifyDataSetChanged()} must be called
	 * whenever {@link #mObjects} is modified.
	 */
	private boolean mNotifyOnChange = true;

	/**
	 * Activity context
	 */
	private Context mContext;
	/**
	 * layout inflater
	 */
	protected LayoutInflater mInflater;

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            The current context.
	 * @param resource
	 *            The resource ID for a layout file containing a layout to use
	 *            when instantiating views.
	 */
	protected ArrayObjectAdapter(Context context, int resource) {
		init(context, resource, new ArrayList<T>());
	}

	/**
	 * Constructor. If you use this constructor, you'll have to redefine
	 * {@link #newView(Context, ViewGroup, int)} method.
	 * 
	 * @param context
	 *            The current context.
	 */
	protected ArrayObjectAdapter(Context context) {
		init(context, 0, new ArrayList<T>());
	}

	/**
	 * Adds the specified object at the end of the array.
	 * 
	 * @param object
	 *            The object to add at the end of the array.
	 */
	public void add(T object) {
		mObjects.add(object);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Adds the specified Collection at the end of the array.
	 * 
	 * @param collection
	 *            The Collection to add at the end of the array.
	 */
	public void addAll(Collection<? extends T> collection) {
		mObjects.addAll(collection);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Adds the specified items at the end of the array.
	 * 
	 * @param items
	 *            The items to add at the end of the array.
	 */
	public void addAll(T... items) {
		for (T item : items) {
			mObjects.add(item);
		}
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Inserts the specified object at the specified index in the array.
	 * 
	 * @param object
	 *            The object to insert into the array.
	 * @param index
	 *            The index at which the object must be inserted.
	 */
	public void insert(T object, int index) {
		mObjects.add(index, object);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	public void update(T object, int index) {
		mObjects.set(index, object);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Removes the specified object from the array.
	 * 
	 * @param object
	 *            The object to remove.
	 */
	public void remove(T object) {
		mObjects.remove(object);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	public void remove(int position) {
		mObjects.remove(position);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Remove all elements from the list.
	 */
	public void clear() {
		mObjects.clear();
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Sorts the content of this adapter using the specified comparator.
	 * 
	 * @param comparator
	 *            The comparator used to sort the objects contained in this
	 *            adapter.
	 */
	public void sort(Comparator<? super T> comparator) {
		Collections.sort(mObjects, comparator);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		mNotifyOnChange = true;
	}

	/**
	 * Control whether methods that change the list ({@link #add},
	 * {@link #insert}, {@link #remove}, {@link #clear}) automatically call
	 * {@link #notifyDataSetChanged}. If set to false, caller must manually call
	 * notifyDataSetChanged() to have the changes reflected in the attached
	 * view.
	 * 
	 * The default is true, and calling notifyDataSetChanged() resets the flag
	 * to true.
	 * 
	 * @param notifyOnChange
	 *            if true, modifications to the list will automatically call
	 *            {@link #notifyDataSetChanged}
	 */
	public void setNotifyOnChange(boolean notifyOnChange) {
		mNotifyOnChange = notifyOnChange;
	}

	/**
	 * initialize list
	 * 
	 * @param context
	 * @param resource
	 * @param objects
	 */
	private void init(Context context, int resource, ArrayList<T> objects) {
		mContext = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mResource = resource;
		mObjects = objects;
	}

	/**
	 * Returns the context associated with this array adapter. The context is
	 * used to create views from the resource passed to the constructor.
	 * 
	 * @return The Context associated with this adapter.
	 */
	public Context getContext() {
		return mContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getCount() {
		if (mObjects != null) {
			return mObjects.size();
		} else {
			return 0;
		}
	}

	@Override
	public T getItem(int position) {
		if (mObjects != null) {
			if ((position >= 0) && (position < mObjects.size())) {
				return mObjects.get(position);
			}
		}
		return null;
	}

	/**
	 * Returns the position of the specified item in the array.
	 * 
	 * @param item
	 *            The item to retrieve the position of.
	 * 
	 * @return The position of the specified item.
	 */
	public int getPosition(T item) {
		return mObjects.indexOf(item);
	}

	/**
	 * {@inheritDoc}
	 */
	public long getItemId(int position) {
		return position;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View currentView;
		final Context context = getContext();
		if (convertView == null)
			currentView = newView(context, parent, position);
		else
			currentView = convertView;
		bindView(currentView, context, getItem(position), position);

		return currentView;
	}

	/**
	 * create new widget object for list
	 * 
	 * @param context
	 * @param parent
	 * @return
	 */
	protected View newView(Context context, ViewGroup parent, int position) {
		if (mResource == 0) {
			throw new RuntimeException(
					"newView method must be redifined if no resourceId is used.");
		} else {
			return mInflater.inflate(mResource, parent, false);
		}
	}

	/**
	 * bind a view with an existing widget
	 * 
	 * @param view
	 * @param context
	 * @param obj
	 */
	protected void bindView(View view, Context context, T obj, int position) {
	};

	/**
	 * getter on all items of the adapter
	 * 
	 * @return all items
	 */
	public ArrayList<T> getItems() {
		return mObjects;
	}

	/**
	 * getter on some items of the adapter at some positions
	 * 
	 * @return items at desired positions
	 */
	public ArrayList<T> getItems(ArrayList<Integer> positions) {
		ArrayList<T> result = new ArrayList<T>();
		for (int i = 0; i < mObjects.size(); i++) {
			if (positions.contains(i)) {
				result.add(mObjects.get(i));
			}
		}
		return result;
	}

	/**
	 * getter on all items of the adapter
	 * 
	 * @param items
	 */
	public void setItems(ArrayList<T> items) {
		mObjects = items;
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}
}
