package com.sample.pinterest.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.framework.mozart.IResponse;
import com.framework.mozart.ImageDataLoader;
import com.framework.mozart.error.MozartError;
import com.sample.pinterest.R;
import com.sample.pinterest.data.ImageData;
import com.sample.pinterest.data.ProductData;

public class ProductAdapter extends ArrayObjectAdapter<ProductData> {

	private Context mContext;

	public ProductAdapter(Context context) {
		super(context, R.layout.grid_item);
		this.mContext = context;

	}

	@Override
	protected View newView(Context context, ViewGroup parent, int position) {

		ViewHolder holder = new ViewHolder();
		View view = super.newView(context, parent, position);
		holder.description = (TextView) view.findViewById(R.id.title);
		holder.imageView = (ImageView) view.findViewById(R.id.image);
		view.setTag(holder);
		return view;

	}

	@Override
	protected void bindView(View view, Context context, ProductData obj,
			int position) {

		super.bindView(view, context, obj, position);
		if (view != null) {
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.description.setText(obj.description);
			if (obj.images != null) {
				for (ImageData imageData : obj.images) {
					String url = imageData.url.replace("\\", "");
					initImageLoading(holder.imageView, url, imageData.width,
							imageData.height);
				}
			}
		}
	}

	private Bitmap initImageLoading(final ImageView view, String url,
			int width, int height) {
		ImageDataLoader loader = new ImageDataLoader(this.mContext, url, true,
				width, height);
		IResponse<Bitmap> responseListener = new IResponse<Bitmap>() {

			@Override
			public void OnError(MozartError error) {
				if (error != null) {
					if (error.message != null)
						Toast.makeText(mContext, error.message,
								Toast.LENGTH_SHORT).show();
				}

			}

			@Override
			public void OnSuccess(Bitmap response) {
				view.setImageBitmap(response);

			}
		};
		loader.setResponseListener(responseListener);
		loader.build();
		return null;
	}

	class ViewHolder {
		public TextView description;
		public ImageView imageView;

	}

}
