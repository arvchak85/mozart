package com.sample.pinterest.data;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataHelper {

	public DataHelper() {

	}

	public ArrayList<ProductData> buildData(JSONObject jsonObject) {

		ArrayList<ProductData> productData = new ArrayList<ProductData>();

		try {
			JSONObject metadata = (JSONObject) jsonObject.get("metadata");
			JSONArray data = metadata.getJSONArray("data");
			for (int i = 0; i < data.length(); i++) {
				JSONObject innerObj = (JSONObject) data.get(i);
				JSONObject attributes = (JSONObject) innerObj.get("attributes");
				String description = attributes.getString("description");
				JSONArray imageList = attributes.getJSONArray("image_list");
				ArrayList<ImageData> imageDataList = new ArrayList<ImageData>();
				for (int iterator = 0; iterator < imageList.length(); iterator++) {
					ImageData imageData = new ImageData();
					JSONObject innerArray = (JSONObject) imageList
							.get(iterator);
					imageData.height = innerArray.getInt("height");
					imageData.width = innerArray.getInt("width");
					imageData.url = innerArray.getString("url");
					imageDataList.add(imageData);
				}
				productData.add(new ProductData(description, imageDataList));

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return productData;
	}

}
