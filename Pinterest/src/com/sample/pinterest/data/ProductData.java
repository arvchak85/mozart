package com.sample.pinterest.data;

import java.util.ArrayList;

public class ProductData {

	public String description;
	public ArrayList<ImageData> images;

	public ProductData(String description, ArrayList<ImageData> images) {
		super();
		this.description = description;
		this.images = images;
	}

}
